﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class CCR
    {
        public int  Id { get; set; }

        [Required(ErrorMessage ="Enter Project Name")]
        public string ProjectName { get; set; }

        [Required(ErrorMessage ="Enter Bank Name")]
        public string BankName { get; set; }

        [Required(ErrorMessage ="Enter LOC No")]
        public string LOCNo { get; set; }

        [Required(ErrorMessage ="Enter Beneficiary")]
        public string Beneficiary { get; set; }

        [Required(ErrorMessage ="Enter Survey/Gate No")]
        public string SurveyGateNo { get; set; }

        [Required(ErrorMessage ="Enter LOT No")]
        public string LOTNo { get; set; }

        [Required(ErrorMessage ="Select Type")]
        public string Type { get; set; }

        [Required(ErrorMessage ="Select CCR For")]
        public string CCRFor { get; set; }

        [Required(ErrorMessage ="Enter Valuation")]
        public string Valuation { get; set; }

        [Required(ErrorMessage ="Select Checque No")]
        public string ChequeNo { get; set; }

        [Required(ErrorMessage ="Select Cheque Date")]
        public string ChequeDate { get; set; }

        public string ChequeClearingDate { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        //[Required(ErrorMessage ="Select Roll Date")]
        public string RollDate { get; set; }

        //[Required(ErrorMessage ="Enter Roll No")]
        public string RollNo { get; set; }

        public string Remark { get; set; }
        
        //public string IsActive { get; set; }
        public string CreatedDate { get; set;}
        public string ModifiedDate { get; set;}
    }
}
