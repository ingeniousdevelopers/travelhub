﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Please enter valid First Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid First Name")]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter valid Last Name")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid Last Name")]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter valid Middle Name")]
        [DataType(DataType.Text)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Please enter valid Email ID")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter valid Password with minimum 6 characters")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Please enter valid Password with minimum 6 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password",
            ErrorMessage = "Password and confirmation password do not match")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Please enter valid Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid Mobile Number")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        public string StatusMessage { get; set; }

        [Required(ErrorMessage = "Please choose valid Standard")]
        [DataType(DataType.Text)]
        [Display(Name = "Standard")]
        public string Standard { get; set; }

        public int BoardID { get; set; }
        public int MediumID { get; set; }
        public int StandardID { get; set; }
        public int SubjectID { get; set; }

    }
}
