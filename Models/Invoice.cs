﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Invoice
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Customer Name")]
        public string CustomerName { get; set; }


        [Required(ErrorMessage = "Select Project Name")]
        public string ProjectName { get; set; }

        [Required(ErrorMessage = "Select Invoice Date")]
        public string InvoiceDate { get; set; }

        [Required(ErrorMessage = "Select Bill To")]
        public string BillTo { get; set; }


        [Required(ErrorMessage = "Enter PanCard No")]
        public string PanCardNo { get; set; }

        //[Required(ErrorMessage ="Enter Advance Payment")]
        public string AdvancePayment { get; set; }
        public string TotalAmount { get; set; }
        public string GSTHeading { get; set; }

        public string CGSTHeading { get; set; }
        public string SGSTHeading { get; set; }
        public string IGSTHeading { get; set; }

        public string RHeading { get; set; }


        public string GSTValue { get; set; }
        public string CGSTValue { get; set; }
        public string SGSTValue { get; set; }
        public string IGSTValue { get; set; }

        public string RValue { get; set; }

        public string GSTPercentage { get; set; }
        public string CGSTPercentage { get; set; }
        public string SGSTPercentage { get; set; }
        public string IGSTPercentage { get; set; }

        public string RPercentage { get; set; }

        public string TDSHeading { get; set; }
        public string TDSValue { get; set; }
        public string TDSPercentage { get; set; }


        public string CessHeading { get; set; }
        public string CessValue { get; set; }

        public string CessPercentage { get; set; }

        public string TotalPaybleAmount { get; set; }

        public string Description { get; set; }

        public string HSNCode { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public string Amount { get; set; }

        public string InvoiceNumber { get; set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public string IsActive { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string RoundOffType { get; set; }
        public double RoundOff { get;  set; }
    }
}
