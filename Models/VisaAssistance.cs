﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class VisaAssistance
    {
        [Required]
        public int ID { get; set; }

        public int LocID { get; set; }

        public int VAID { get; set; }


        [Required]
        public String Continent { get; set; }

        public String Description { get; set; }

        public String CountryName { get; set; }

        public String GuestProfession { get; set; }

        public String VisaType { get; set; }

        public String Duration { get; set; }

        public String Fees { get; set; }

        public String SOP { get; set; }

        public String ImportantNote { get; set; }

    }
}


