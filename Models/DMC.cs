﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class DMC
    {
        [Required]
        public int ID { get; set; }

        public int LocID { get; set; }

        public int DestID { get; set; }


        [Required]
        public String CompanyName { get; set; }

        public String GroupOf { get; set; }

        public String ContactType { get; set; }

        public String SectorName { get; set; }

        public String Speciality { get; set; }

        public String SpecificSp { get; set; }

        public String ServiceIn { get; set; }


        public String ContactName { get; set; }


        public String ContactDesignation { get; set; }

        public String ContactLandLine { get; set; }

        public String ContactMobile { get; set; }

        public String ContactMail { get; set; }

        public String ContactPerMob { get; set; }

        public String ContactWorkProfile { get; set; }

        public String EmergencyContact { get; set; }

        public String Address1 { get; set; }
        public String Address2 { get; set; }

        public int CompanyFrom { get; set; }

        public String CategoryName { get; set; }


        public String ReferenceName { get; set; }
        public String AccrediationName { get; set; }

        public String BankName { get; set; }



        public String AccountNo { get; set; }

        public String IFSC { get; set; }
        public String Branch { get; set; }

        public String ReservationContact { get; set; }
        public String OwnFleets { get; set; }


        public String Grade { get; set; }

        public int DID { get; internal set; }
    }
}


