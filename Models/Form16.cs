﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Form16
    {
        public int RRID { get; set; }
        public int RebateID { get; set; }
        public int AID { get; set; }
        public int FormID { get; set; }
        public int RFAID { get; set; }
        public int RID { get; set; }
        public int RDID { get; set; }
        public int EmployeeID { get; set; }
        public int RGID { get; set; }
        public int GID { get; set; }

        public int REIID { get; set; }

        public int RSSID { get; set; }

        public int DeductionID { get; set; }

        public int CDeductionID { get; set; }
        public int RDVIAID { get; set; }

        public int OIncomeID { get; set; }
        public int SectionID { get; set; }

        public int CompanyId { get; set; }

        public string Description { get; set; }

        public string TotalAllowance { get; set; }
        public string TotalGross { get; set; }
        public string Status { get; set; }
        public string AssessmentYear { get; set; }
        public string DAmount { get; set; }
        public string QAmount { get; set; }
        public string GrossAmount { get; set; }
        public int SecNo { get; set; }
        public string TAmount { get; set; }
        
        public string TotalDeductions { get; set; }


        public string TotalOtherIncome { get; set; }

        public string TotalDecChapterVI { get; set; }
        public string TextFooter { get; set; }
        public string TotalPaid { get; set; }
        public string place { get; set; }

        public string fullname { get; set; }
        public string designation { get; set; }
        public string date { get; set; }
        public string TaxableIn { get; set; }


        public string CompPAN { get; set; }
        public string CompTAN { get; set; }

        public string TaxOnTotalInc { get; set; }

        public string Section88B { get; set; }

        public string Section88C { get; set; }

        public string Section88D { get; set; }
        public string Amount { get; set; }
        public int CreatedBy { get; set; }
        public string ModifiedBy { get; set; }


        public string StartDate { get; set; }
        public string EndDate { get; set; }


        public string CompanyName { get; set; }
        public string EmployeeName { get; set; }
        

        public string IsActive { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
    }
}
