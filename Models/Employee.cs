﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Employee
    {

        public int ID { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Range(0,1)]
        public int IsAccountActive { get; set; }

        public String Picture { get; set; }

        public DateTime CreatedOn { get; set; }
        public string Name { get;  set; }

        public String Date { get; set; }

        [Required(ErrorMessage = "Please enter valid Email ID")]
        [EmailAddress]
        public String Email { get; set; }

        [Required(ErrorMessage = "Please enter valid Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid Mobile Number")]
        [Display(Name = "Phone number")]
        public String PhoneNumber { get; set; }
        public String Role { get; set; }

        public int RoleID { get; set; }

    }
}
