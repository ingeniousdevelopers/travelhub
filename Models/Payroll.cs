﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Payroll
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        [Required(ErrorMessage = "Select Company Name")]
        public int CompanyId { get; set; }
        
        public string CompanyName { get; set; }
        public string ProjectName { get; set; }
        public string LeaveDate { get; set; }
        public string PanCardFile { get; set; }
        public string AadharCardFile { get; set; }
        public string BankDetailsFile { get; set; }
    
        [Required(ErrorMessage = "Enter First Name")]
        public string FirstName { get; set; }

        public string MiddleName { get; set; }


        [Required(ErrorMessage = "Enter Last Name")]
        public string LastName { get; set; }


        [Required(ErrorMessage = "Enter Valid Email Address")]
        [EmailAddress]
        public string Email { get; set; }


        [Required(ErrorMessage = "Enter Valid Phone No")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Mobile No is not valid")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Enter Valid Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Select Date Of Birth")]
        public string DOB { get; set; }

        [Required(ErrorMessage = "Select Joining Date")]
        public string JoiningDate { get; set; }

        [Required(ErrorMessage = "Enter Designation")]
        public string Designation { get; set; }

        [Required(ErrorMessage = "Enter total salary per month")]
        public string PerMonthSalary { get; set; }

        //[Required(ErrorMessage = "Enter payable basic salary")]
        public string PaybleBasicSalary { get; set; }

        //[Required(ErrorMessage = "Enter payable consolidated allowances ")]
        public string PaybleAllowances { get; set; }

        [Required(ErrorMessage = "Enter bank name")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Enter bank account number")]
        public string BankAccountNo { get; set; }

        [Required(ErrorMessage = "Enter IFSC code")]
        public string IFSCCode { get; set; }


        public string PerMonthBasicSalary { get; set; }
        public string AllowancesPerMonth { get; set; }

        //[Required(ErrorMessage ="Enter Designation")]
        public string IDCardNumber { get; set; }

        [Required(ErrorMessage = "Enter Pan card number")]
        public string PanCardNumber { get; set; }


        [Required(ErrorMessage = "Enter Aadhar card number")]
        public string AadharCardNumber { get; set; }

        public string EmployeeName { get; set; }

        [Required(ErrorMessage = "Select Month")]
        public string Month { get; set; }

        [Required(ErrorMessage = "Select Year")]
        public string Year { get; set; }

        [Required(ErrorMessage ="Enter LOP")]
        public string LOP { get; set; }

        [Required(ErrorMessage = "Select Professional Tax")]
        public string PT { get; set; }

        [Required(ErrorMessage = "Enter Payable Salary")]

        public string PayableSalary { get; set; }

        public string PF { get; set; }
        public string TDS { get; set; }
        public string ESIC { get; set; }
        public string ApecentationDeduction { get; set; }
        public string LoanDeduction { get; set; }


        public string SalaryType { get; set; }
        public string Amount { get; set; }
        public string Remark { get; set; }
        public string WEF { get; set; }


        public string IsActive { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }


        public List<SalaryLastDateModel> SalaryLastDate { get; set; }
    }
}
