﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class RABill
    {
        [Required]
        public int ID { get; set; }

        public int DeductionID { get; set; }

        [Required]
        public String ClientName { get; set; }

        public String ClientAddress { get; set; }

        public String Consignee { get; set; }

        public String ConsigneeAddress { get; set; }

        public String Date { get; set; }

        public String InvoiceNo { get; set; }

        public String BillDetails { get; set; }
        public String SQPAN { get; set; }

        public String SQGST { get; set; }
        public int CompanyID { get; set; }
        public String CPAN { get; set; }

        public String CGST { get; set; }

        public String LOANO { get; set; }
        public String ProjectName { get; set; }

        public String Description { get; set; }

        public String Unit { get; set; }

        public Double LOAQty { get; set; }

        public Double AvaQty { get; set; }


        public Double ClaimQty { get; set; }
        public Double PassedQty { get; set; }
        public Double EWR { get; set; }



        public Double Amount { get; set; }
        public double GrossInvoiceAmt { get; internal set; }
        public string CreatedBy { get; internal set; }
        public double TotalGST { get; internal set; }
        public double TotalCGST { get; internal set; }
        public double TotalSGST { get; internal set; }
        public double TotalLWC { get; internal set; }
        public double ClaimedQty { get; internal set; }
        public double ExWorks { get; internal set; }
        public int ProjectID { get; internal set; }
        public int TypeID { get; internal set; }
        public string CompanyName { get; internal set; }
        public double UnitRate { get; internal set; }
        public double TotalClaimedQty { get; internal set; }
        public double TotalPassedQty { get; internal set; }
        public double BalanceQty { get; internal set; }
        public int DescID { get; internal set; }


        public string Email { get; set; }
        public string LogoFile { get; set; }
        public string TelNo { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }

        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BankAccountNo { get; set; }
        public string IFSCCode { get; set; }
        public int IsAvailable { get; internal set; }
        public int IsAvailable2 { get; internal set; }
        public string Heading { get; internal set; }
        public double NetR { get; internal set; }
        public string HSN { get; internal set; }

        public List<RABill> Deductions { get; set; }
        public string RoundOffType { get; internal set; }
        public double RoundOff { get; internal set; }
    }
}
