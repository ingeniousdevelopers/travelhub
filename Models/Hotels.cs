﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Hotels
    {
        [Required]
        public int ID { get; set; }

        public int LocID { get; set; }

        public int DestID { get; set; }


        [Required]
        public String Name { get; set; }

        public String GroupOf { get; set; }

        public String SectorName { get; set; }

        public String CountryName { get; set; }

        public String StateName { get; set; }

        public String Address1 { get; set; }

        public String Address2 { get; set; }
        public String Address1T { get; set; }
        public String Address1C { get; set; }
        public String Address2T { get; set; }

        public String Address2C { get; set; }
        public String Star { get; set; }

        public String Meal { get; set; }

        public String IsFour { get; set; }

        public int CompanyFrom { get; set; }

        public String CategoryName { get; set; }


        public String ReferenceName { get; set; }
        public String AccrediationName { get; set; }

        public String BankName { get; set; }

        public String ContactName { get; set; }


        public String ContactDesignation { get; set; }

        public String ContactLandLine { get; set; }

        public String ContactMobile { get; set; }

        public String ContactMail { get; set; }

        public String ContactPerMob { get; set; }

        public String ContactWorkProfile { get; set; }


        public String AccountNo { get; set; }

        public String IFSC { get; set; }
        public String Branch { get; set; }

        public String ReservationContact { get; set; }
        public String OwnVisit { get; set; }


        public String RoomTypes { get; set; }
        public String Grade { get; set; }

        public String Tarrif { get; set; }

        public String HotelCat { get; set; }
        public Double ExtraAdultCharges { get; set; }

        public Double ExtraChildCharges { get; set; }
        public Double ExtraMealCharges { get; set; }
        public int HID { get; internal set; }
    }
}


