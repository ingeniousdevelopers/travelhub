﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Models
{
    public class Project
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public String Name { get; set; }

        public String Description { get; set; }

        public Double FI { get; set; }
        public Double GST { get; set; }

        public Double CESS { get; set; }

        public int DescriptionID { get; set; }
        public int UnitID { get; set; }
        public int Quantity { get; set; }
        public double EGST { get; internal set; }
        public double ECESS { get; internal set; }
        public string Date { get; internal set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string FullName { get; internal set; }
        public string Status { get; internal set; }
        public string CreatedBy { get; internal set; }

        public String SQPAN { get; set; }

        public String SQGST { get; set; }

        public int CompanyID { get; set; }
        public String CPAN { get; set; }

        public String CGST { get; set; }

        public String LOANO { get; set; }
        public String ClientName { get; set; }

        public String ClientAddress { get; set; }

        public String Consignee { get; set; }

        public String ConsigneeAddress { get; set; }


        public String InvoiceNo { get; set; }
        public string CompanyName { get;  set; }
        public int IsEditable { get;  set; }

        public List<RABill> Bill { get; set; }

    }
}
