﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TravelHub.Models
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage ="Enter Valid Email Address")]
        [EmailAddress]
        [Remote(action: "IsValidateEmail", controller: "Auth")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter valid Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid Mobile Number")]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }
}
