﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using TravelHub.Data;
using TravelHub.Models;
using TravelHub.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Data
{
    public class DbInitializer : IDbInitializer
    {
        private readonly ApplicationDbContext _db;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public DbInitializer(ApplicationDbContext db, UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager)
        {
            _db = db;
            _roleManager = roleManager;
            _userManager = userManager;
        }


        public async void Initialize()
        {
            try
            {
                if (_db.Database.GetPendingMigrations().Count() > 0)
                {
                    _db.Database.Migrate();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            if (_db.Roles.Any(r => r.Name == Constants.Admin)) return;

            _ = _roleManager.CreateAsync(new ApplicationRole(Constants.Admin)).GetAwaiter().GetResult();

            _ =  _userManager.CreateAsync(new ApplicationUser
            {
                //UserName = "info@square7.in",
                //Email = "info@square7.in",
                //EmailConfirmed = true,
                //PhoneNumber = "0000000000",
                //FirstName = "Square",
                //LastName  = "Seven",
                //PhoneNumberConfirmed = true,
                //IsAccountActive = 1,
                //CreatedOn = DateTime.Now

                UserName = "chetanmali005@gmail.com",
                Email = "chetanmali005@gmail.com",
                EmailConfirmed = true,
                PhoneNumber = "0000000001",
                FirstName = "Chetan",
                LastName = "Mali",
                PhoneNumberConfirmed = true,
                IsAccountActive = 1,
                CreatedOn = DateTime.Now

            }, "Landmark@2").GetAwaiter().GetResult();


            ApplicationUser user = await _db.Users.FirstOrDefaultAsync(u => u.Email == "chetanmali005@gmail.com");


            await _userManager.AddToRoleAsync(user, Constants.Admin);

        }

    }
}