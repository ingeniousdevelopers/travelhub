﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Data
{
    public class Dblib
    {
        public enum Connections { LocalDefault };

        public SqlConnection defConnection;
        // public SqlConnection empConnection;
        public static int currentEno;
        private readonly IConfiguration _config;

        public Dblib(IConfiguration config)
        {
            _config = config;
        }

        public Dblib()
        {

        }
        public SqlConnection getDefConnection()
        {
            return new SqlConnection(Startup.defCon);
        }
        //public SqlConnection getEmpConnection()
        //{
        //   return new SqlConnection(_config.GetConnectionString("ServerString"));
        //}

        public object getScalar(string sqlQry, Connections conType)
        {
            SqlConnection con;
            object obj = null;

            try
            {
                con = this.getDefConnection();

                con.Open();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = sqlQry;
                obj = cmd.ExecuteScalar();
                cmd.Dispose();
                con.Close();
                return obj;
            }
            catch
            {
                return obj;

            }
            finally
            {

            }
        }


        public object getScalar(SqlCommand cmd, Connections conType)
        {
            SqlConnection con;
            object obj = null;

            try
            {

                con = this.getDefConnection();
                con.Open();

                cmd.Connection = con;
                obj = cmd.ExecuteScalar();
                cmd.Dispose();
                con.Close();
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return obj;

            }
            finally
            {

            }
        }

        public bool hasRecord(string sqlQry, Connections conType)
        {
            SqlConnection con;
            bool flag = false;

            try
            {
                con = this.getDefConnection();
                con.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText = sqlQry;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                    flag = true;
                dr.Dispose();
                cmd.Dispose();
                con.Close();
                return flag;
            }
            catch
            {
                return flag;

            }
            finally
            {

            }
        }

        //public string getUserName(string userID)
        //{
        //    string userName = "";
        //    string sqlQry = "select [EmployeeName]  FROM [Tablename] where ntid='" + userID + "'";
        //    if (hasRecord(sqlQry, Connections.HRCore))
        //    {
        //        object obj = this.getScalar(sqlQry, Connections.HRCore);
        //        userName = obj.ToString();
        //    }
        //    if (string.IsNullOrEmpty(userName))
        //    {
        //        userName = "Invalid User";
        //    }
        //    return userName;
        //}


        //public string getUserNameForCustomization(string userID)
        //{
        //    string userName = "";
        //    string[] Login1 = userID.Split('\\');
        //    string main_user = Login1[0];

        //    string sqlQry = "select [EmployeeName]  FROM [Employeeservices].[dbo].[VEmployeeAll] where ntid='" + main_user + "'";
        //    if (hasRecord(sqlQry, Connections.HRCore))
        //    {
        //        object obj = this.getScalar(sqlQry, Connections.HRCore);
        //        userName = obj.ToString();
        //    }
        //    if (string.IsNullOrEmpty(userName))
        //    {
        //        userName = "-";
        //    }
        //    return userName;
        //}


        public string getDate(string date1)
        {
            //  string[] date_array = date1.Split('/');
            // date1 = date_array[2] + '-' + date_array[1] + '-' + date_array[0];

            // if (date1 =="")
            // {
            //     date1 ="";
            //     return date1;
            // }

            if (date1.Contains('/'))
            {
                string[] date_array = date1.Split('/');

                    date1 = date_array[2].Substring(0, 4) + '-' + date_array[1] + '-' + date_array[0];
                    return date1;

            }
            else
            {
                string[] date_array = date1.Split('-');

                date1 = date_array[0].Substring(0, 4) + '-' + date_array[1] + '-' + date_array[2];
                return date1;

            }
        

        }


        public string getDateNew(string date1)
        {
            //  string[] date_array = date1.Split('/');
            // date1 = date_array[2] + '-' + date_array[1] + '-' + date_array[0];

             //Console.WriteLine(date1);
            if (date1 =="")
            {
                date1 ="";
                return date1;
            }
            
            if (date1.Contains('/'))
            {
                string[] date_array = date1.Split('/');

                    date1 = date_array[2].Substring(0, 4) + '-' + date_array[1] + '-' + date_array[0];
                    return date1;

            }
            else
            {
                string[] date_array = date1.Split('-');

                date1 = date_array[0].Substring(0, 4) + '-' + date_array[1] + '-' + date_array[2];
                return date1;

            }
            
        

        }



        //public int getAuthentication(string userID)
        //{
        //    int Menu = 0;
        //    SqlConnection con = this.getDefConnection();
        //    con.Open();
        //    SqlCommand cmdMenu = new SqlCommand();
        //    cmdMenu.CommandType = CommandType.StoredProcedure;
        //    cmdMenu.CommandText = "USP_HP_NAP_SELECT";
        //    cmdMenu.Parameters.AddWithValue("@SELECTSTEP", "Check_Authorization");
        //    cmdMenu.Parameters.AddWithValue("@Col3", userID);
        //    cmdMenu.Connection = con;
        //    SqlDataReader drMenu = cmdMenu.ExecuteReader();
        //    if (drMenu.Read())
        //    {
        //        Menu = int.Parse(drMenu["Menu_ID"].ToString());
        //    }
        //    con.Close();
        //    if (Menu != 0)
        //    {
        //        return Menu;
        //    }
        //    return 0;
        //}

    }
}
