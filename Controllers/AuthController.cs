﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using TravelHub.Models;
using TravelHub.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using System.Net.Mail;
using System.IO;

namespace TravelHub.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {


        private readonly ILogger<HomeController> _logger;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext _db;
        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IConfiguration _config;

        private SmtpClient smtpClient; 
     
        [TempData]
        public string Message { get; set; }
        public AuthController(ILogger<HomeController> logger, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,RoleManager<ApplicationRole> roleManager, ApplicationDbContext _db, IConfiguration config, SmtpClient smtpClient)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this._db = _db;
            _config = config;
            _logger = logger;
            this.smtpClient = smtpClient;

        }


        public IActionResult Index()
        {
            return View();
        }

        //public IActionResult Register()
        //{
        //    return View();
        //}


        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> Index(LoginViewModel model, String returnUrl)
        {




            if (ModelState.IsValid)
            {
          
                var user = await userManager.FindByEmailAsync(model.Email);

                if (user == null)
                {
              
                    ModelState.AddModelError(string.Empty, "Invalid Login!");
                    return View(model);

                }

                var result = await signInManager.PasswordSignInAsync(
                model.Email, model.Password, true, false);

                if (result.Succeeded)
                {

                        //int hasAccess = CheckAccess(user.Id);
                        //if (hasAccess == 0)
                        //{
                        //    await signInManager.SignOutAsync();
                        //    ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
                        //    return View();
                        //}
                        return RedirectToAction("Dashboard", "Admin");
                    
 

                }




                ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
            }

            return View(model);
        }



        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Dashboard() {

            return View();

        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            //Console.WriteLine(ModelState.IsValid);
            // if (ModelState.IsValid)
            // {
                
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);
                
                if (user != null )
                {
                    // Generate the reset password token
                    var token = await userManager.GeneratePasswordResetTokenAsync(user);

                    // Build the password reset link
                    var passwordResetLink = Url.Action("ResetPassword", "Auth",
                            new { email = model.Email, token = token }, Request.Scheme);

                    // Log the password reset link
                    //logger.Log(LogLevel.Warning, passwordResetLink);
                    
                    Console.WriteLine(passwordResetLink);
                    using (var memoryStream = new MemoryStream())
                    {
                        MailMessage mail = new MailMessage();
                        mail.To.Add(model.Email);
                        mail.From = new MailAddress("TravelHubnashik@gmail.com");
                        mail.Subject = "Password Reset Link";
                        mail.Body =  "Hello " +  user.FirstName + ", <br /><br />Please click on below link to reset your password.  <br />" + passwordResetLink + "  <br /><br />Regards, <br />Team TravelHub";
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                        smtp.EnableSsl = true;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("TravelHubnashik@gmail.com", "Square7@2021");
                        smtp.Send(mail);
                    }

                    // var client = new SmtpClient("smtp.gmail.com", 587)
                    // {
                    //     Credentials = new System.Net.NetworkCredential("kalpesh33patil@gmail.com", "Kalpesh@78"),
                    //     EnableSsl = true
                    // };
                    // client.Send("kalpesh33patil@gmail.com", "kalpesh33patil@gmail.com", "Password Reset Link", "Hello " +  user.FirstName + ", <br /><br />Please click on below link to reset your password.  <br />" + passwordResetLink + "  <br /><br />Regards, <br />Team TravelHub");
                    Console.WriteLine("Sent");
                    
                    // Send the user to Forgot Password Confirmation view
                    return View("ForgotPasswordConfirmation");
                }

               //ModelState.AddModelError(nameof(model.Email), "Invalid Email Details");
                    // Message ="";
                    // Message = "Invalid Email Details";
                    // return RedirectToAction("ForgotPassword","Auth");
            // }
            // else
            // {
            //         Message ="";
            //         Message = "Invalid Email Details";
            //         return RedirectToAction("ForgotPassword","Auth");
            // }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> IsValidateEmail(string Email)
        {
            
             var user = await userManager.FindByEmailAsync(Email);

            if (user != null)
            {
                return Json(true);
            }
            else
            {
                return Json($"Email {Email} is invalid.");
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
               return View();
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token, string email)
        {
            if (token == null || email == null)
            {
                ModelState.AddModelError("", "Invalid password reset token");
            }
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            // if (ModelState.IsValid)
            // {
                // Find the user by email
                var user = await userManager.FindByEmailAsync(model.Email);

                if (user != null)
                {
                    // reset the user password
                    var result = await userManager.ResetPasswordAsync(user, model.Token, model.Password);
                    if (result.Succeeded)
                    {
                        Message ="";
                        Message = "Password Reset Successfully!";
                        return RedirectToAction("Index","Auth");
                    }
                    // Display validation errors. For example, password reset token already
                    // used to change the password or password complexity rules not met
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                    return View(model);
                }

                 //return RedirectToAction("Index","Auth");
            // }
            // Display validation errors if model state is not valid
            return View(model);
        }

        [Authorize]
        public async Task<IActionResult> SignOut()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Auth");
        }
        public int CheckAccess(int Id)
        {


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            int IsExist = 0;

            try
            {
                conn.Open();
                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "ManageAdmin_Access";
                cmdB.Parameters.AddWithValue("@varStepName", "Check_User_Access");
                cmdB.Parameters.AddWithValue("@varUserID", Id);

                cmdB.Connection = conn;

                SqlDataReader drB = cmdB.ExecuteReader();

                while (drB.Read())
                {

                    IsExist = int.Parse( drB["IsAccountActive"].ToString());

                }


                drB.Dispose();
                cmdB.Dispose();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            }

            return IsExist;

        }



        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
