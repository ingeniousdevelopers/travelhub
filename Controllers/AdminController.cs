﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TravelHub.Models;
using System.Linq;
using TravelHub.Utility;
using System;
using System.Data;
using TravelHub.Data;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using TravelHub.Services;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using System.Text.RegularExpressions;

namespace TravelHub.Controllers
{
    //[Authorize(Roles = Constants.Admin)]
    public class AdminController : Controller
    {



        //public AdminController(IConfiguration configuration,ILogger<AdminController> logger,ApplicationDbContext db)
        //{
        //    _logger = logger;
        //    this.db = db;
        //}


        private readonly ILogger<AdminController> _logger;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext _db;
        private readonly string _connectionString;

        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IConfiguration _config;
        private readonly CDNDetails CDNSettings;

        [TempData]
        public string Message { get; set; }

        [TempData]
        public string MessageNew { get; set; }
        public object RoundOffType { get; private set; }
        public double RoundOff { get; private set; }

        public AdminController(ILogger<AdminController> logger, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, RoleManager<ApplicationRole> roleManager, ApplicationDbContext _db, IConfiguration config, IOptions<CDNDetails> CDNSettings)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this._db = _db;
            _config = config;
            _logger = logger;
            this.CDNSettings = CDNSettings.Value;
            _connectionString = config.GetConnectionString("DefaultConnection");

        }

        //-----------------------------------------------------------------------CCR-------------------------------------------------------//
        #region 
        public IActionResult Index()
        {
            return View();
        }



        public IActionResult Dashboard()
        {

            //Double TotalSupply = 0.00;
            //Double TotalErection = 0.00;
            //Double TotalSupplyBill = 0.00;
            //Double TotalErectionBill = 0.00;
            //Double TotalSales = 0.00;

            //var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            //System.Globalization.CultureInfo info = System.Globalization.CultureInfo.GetCultureInfo("hi-IN");

            //SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            //conn.Open();
            //Double TotalSales = 0.00;
            //Double TotalBills = 0.00;
            //SqlCommand cmdB = new SqlCommand();
            //cmdB = new SqlCommand();
            //cmdB.CommandType = CommandType.StoredProcedure;
            //cmdB.CommandText = "PROC_Reports_Display";
            //cmdB.Parameters.AddWithValue("@varStepName", "View_Dashboard_Data");

            //cmdB.Connection = conn;

            //SqlDataReader drB = cmdB.ExecuteReader();

            //while (drB.Read())
            //{

            //    ViewBag.TotalSupply = (Double.Parse(drB["TotalSupply"].ToString()).ToString("C2", info));

            //    ViewBag.TotalErection = (Double.Parse(drB["TotalErection"].ToString()).ToString("C2", info));

            //    ViewBag.TotalSupplyBill = (Double.Parse(drB["TotalSupplyBill"].ToString()).ToString("C2", info));

            //    ViewBag.TotalErectionBill = (Double.Parse(drB["TotalErectionBill"].ToString()).ToString("C2", info));

            //    TotalSales = Math.Round(Double.Parse(drB["TotalSupply"].ToString()) + Double.Parse(drB["TotalErection"].ToString()), 2);
            //    TotalBills = Math.Round(Double.Parse(drB["TotalSupplyBill"].ToString()) + Double.Parse(drB["TotalErectionBill"].ToString()), 2);

            //    ViewBag.TotalSales = (TotalSales.ToString("C2", info));
            //    ViewBag.TotalBills = (TotalBills.ToString("C2", info));

            //}

            //drB.Dispose();
            //cmdB.Dispose();
            //conn.Close();
            return View();

        }

   
        //-----------------------------------------------------------------------Billing-------------------------------------------------------//
     



        public IActionResult Locations()
        {
            List<Locations> Locations = new List<Locations>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_SELECT_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Location");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Locations.Add(new Locations
                {
                    ID = int.Parse(drB["ID"].ToString()),
                    LocID = int.Parse(drB["LocID"].ToString()),
                    Type = drB["LocType"].ToString(),
                    Name = drB["Name"].ToString(),
                    SectorName = drB["SectorName"].ToString(),
                    CountryName = drB["CountryName"].ToString(),
                    StateName = drB["StateName"].ToString(),
                    CategoryName = drB["CategoryName"].ToString(),
                    MinStay = Double.Parse(drB["MinStay"].ToString()),
                    TourType = drB["TourType"].ToString(),
                    LocationInfo = drB["LocInfo"].ToString(),

                });
            }

            ViewBag.Locations = Locations;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            return View();

        }

        public IActionResult Hotels()
        {
            List<Hotels> Hotels= new List<Hotels>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Hotels");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Hotels.Add(new Hotels
                {
                    ID = int.Parse(drB["ID"].ToString()),
                    HID = int.Parse(drB["HID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    Name = drB["Name"].ToString(),
                    SectorName = drB["Sector"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address1T = drB["Address1Town"].ToString(),
                    Address1C = drB["Address1Country"].ToString(),
                    Address2T = drB["Address2Town"].ToString(),
                    Address2C = drB["Address2Country"].ToString(),
                    Address2 = drB["Address2"].ToString(),
                    HotelCat = drB["HotelCategory"].ToString(),
                    ExtraAdultCharges = Double.Parse(drB["ExtraAdultCharges"].ToString()),
                    ExtraChildCharges = Double.Parse(drB["ExtraChildCharges"].ToString()),
                    ExtraMealCharges = Double.Parse(drB["ExtraMealCharges"].ToString()),

                    Star = drB["StarCategory"].ToString(),
                    ContactDesignation = drB["ContactDesignation"].ToString(),
                    ContactMail = drB["ContactMailID"].ToString(),
                    ContactMobile = drB["ContactMobile"].ToString(),
                    ContactLandLine = drB["ContactLandLine"].ToString(),
                    ContactPerMob = drB["ContactPerMob"].ToString(),
                    ContactWorkProfile = drB["ContactWorProfile"].ToString(),
                    Meal = drB["MealPlan"].ToString(),
                    IsFour = drB["IsFourPerson"].ToString(),
                    CompanyFrom = int.Parse(drB["CompanyFrom"].ToString()),
                    CategoryName = drB["Category"].ToString(),
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["Reservation Contact"].ToString(),
                    OwnVisit = drB["OwnVisit"].ToString(),
                    RoomTypes = drB["RoomType"].ToString(),
                    Grade = drB["Grade"].ToString(),
                    Tarrif = drB["TarrifPDF"].ToString(),


                });
            }

            ViewBag.Hotels = Hotels;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            return View();

        }


        public IActionResult DMC()
        {
            List<DMC> DMC = new List<DMC>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMC");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                DMC.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    CompanyName = drB["Name"].ToString(),
                    SectorName = drB["SecWork"].ToString(),
                    SpecificSp = drB["SpecificSP"].ToString(),
                    ServiceIn = drB["ServiceIn"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address2 = drB["Address2"].ToString(),

                    OwnFleets = drB["OwnFleets"].ToString(),

                    CompanyFrom = int.Parse(drB["EstFrom"].ToString()),
          
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["EmerContact"].ToString(),
               
                    Grade = drB["Grade"].ToString(),
        

                });
            }

            ViewBag.DMC = DMC;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            return View();

        }


        public IActionResult ViewDMC(int? ID)
        {
            List<DMC> DMC = new List<DMC>();
            List<DMC> Contact = new List<DMC>();
            List<DMC> Bank = new List<DMC>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMC_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                DMC.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    CompanyName = drB["Name"].ToString(),
                    SectorName = drB["SecWork"].ToString(),
                    SpecificSp = drB["SpecificSP"].ToString(),
                    ServiceIn = drB["ServiceIn"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address2 = drB["Address2"].ToString(),

                    OwnFleets = drB["OwnFleets"].ToString(),

                    CompanyFrom = int.Parse(drB["EstFrom"].ToString()),

                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["EmerContact"].ToString(),

                    Grade = drB["Grade"].ToString(),


                });
            }


            ViewBag.DMC = DMC;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMCBank_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Bank.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    BankName = drB["BankName"].ToString(),
                    AccountNo = drB["AccNo"].ToString(),
                    Branch = drB["Branch"].ToString(),
                    IFSC = drB["IFSC"].ToString(),


                });
            }

            ViewBag.Bank = Bank;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();






            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMCContacts_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Contact.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),
                    ContactName = drB["Name"].ToString(),
                    ContactType = drB["Type"].ToString(),

                    ContactDesignation = drB["Designation"].ToString(),
                    ContactMail = drB["MailID"].ToString(),
                    ContactMobile = drB["Mobile"].ToString(),
                    ContactLandLine = drB["OfficeLandLine"].ToString(),
                    ContactPerMob = drB["PersonalMob"].ToString(),
                    ContactWorkProfile = drB["WorkProfile"].ToString(),


                });
            }

            ViewBag.Contacts = Contact;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            return View();
        }


        public IActionResult AddLocation()
        {

            return View();
        }

        public IActionResult AddHotel()
        {

            return View();
        }

        public IActionResult AddDMC()
        {

            return View();
        }

        public IActionResult AddVisa()
        {

            return View();
        }

        public IActionResult VisaAssistance()
        {
            List<VisaAssistance> Visa = new List<VisaAssistance>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_VisaAssistance");

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Visa.Add(new VisaAssistance
                {
                    VAID = int.Parse(drB["VAID"].ToString()),

                    CountryName = drB["CountryName"].ToString(),
                    Continent = drB["Continent"].ToString(),
                    GuestProfession = drB["GuestProfession"].ToString(),
                    VisaType = drB["VisaType"].ToString(),
                    Fees = drB["Fees"].ToString(),
                    SOP = drB["SOP"].ToString(),
                    ImportantNote = drB["ImpNote"].ToString(),

                    Duration = drB["Duration"].ToString(),



                });
            }


            ViewBag.Visa = Visa;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }


        public IActionResult ViewVisa(int ID)
        {
            List<VisaAssistance> Visa = new List<VisaAssistance>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_VisaAssistance_ByID");
            cmdB.Parameters.AddWithValue("@varVAID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Visa.Add(new VisaAssistance
                {
                    VAID = int.Parse(drB["VAID"].ToString()),

                    CountryName = drB["CountryName"].ToString(),
                    Continent = drB["Continent"].ToString(),
                    GuestProfession = drB["GuestProfession"].ToString(),
                    VisaType = drB["VisaType"].ToString(),
                    Fees = drB["Fees"].ToString(),
                    SOP = drB["SOP"].ToString(),
                    ImportantNote = drB["ImpNote"].ToString(),

                    Duration = drB["Duration"].ToString(),



                });
            }


            ViewBag.Visa = Visa;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadProjectFilesAsync()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            var files = HttpContext.Request.Form.Files;

            int ProjectID = int.Parse(Request.Form["projectID"]);

            foreach (var f in files)
            {
                var extentions = f.ContentType;
                if ((extentions == "application/pdf") && f.Length < 5000000)
                {

                }
                else
                {
                    Message = "Failed";
                    return RedirectToAction("ViewProject", new { ID = ProjectID });
                }
            }

            IFormFile file = Request.Form.Files.GetFile("File");


            String FileName = "";
            if (file != null)
            {



                FileName = userId + ProjectID + file.Name + file.FileName;

                CloudBlockBlob blob;
                string connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("projectfiles");
                if (await container.ExistsAsync())
                {
                    FileName = userId + ProjectID + file.Name + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }

                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("projectfiles");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }


            else
            {
                Message = "Failed";
                return RedirectToAction("ViewProject", new { ID = ProjectID });

            }






            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                Dblib dblib = new Dblib();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TravelHub_Add_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_File");
                cmdB.Parameters.AddWithValue("@varProjectID", ProjectID);
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                cmdB.Parameters.AddWithValue("@varFileName", FileName);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();


            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("ViewProject", new { ID = ProjectID });

            }
            finally
            {

                conn.Close();

            }
            Message = "Success";

            return RedirectToAction("ViewProject", new { ID = ProjectID });

        }



        [HttpPost]
        public IActionResult AddLocationDetails(Locations model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            string Type = Request.Form["Type"];
            string Country = Request.Form["Country"];
            string State = Request.Form["StateName"];
            string Sector = Request.Form["SectorName"];
            
            string LocationInfo = Request.Form["LocationInfo"];

            string Location = Request.Form["LocationName"];
            string Category = Request.Form["Category"];
            string TourType = Request.Form["TourType"];
            Double MinStay = Double.Parse(Request.Form["MinStay"]);

            int LocID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Add_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Location");
                cmdB.Parameters.AddWithValue("@varName", Location);
                cmdB.Parameters.AddWithValue("@varSectorName", Sector);
                cmdB.Parameters.AddWithValue("@varCountryName", Country);
                cmdB.Parameters.AddWithValue("@varStateName", State);

                cmdB.Parameters.AddWithValue("@varCategory", Category);
                cmdB.Parameters.AddWithValue("@varMinStay", MinStay);
                cmdB.Parameters.AddWithValue("@varTourType", TourType);
                cmdB.Parameters.AddWithValue("@varType", Type);
                cmdB.Parameters.AddWithValue("@varLocInfo", LocationInfo);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    LocID = int.Parse(drB["LocID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            string[] DestinationName = Request.Form["DestinationName[]"];
            int cnt = DestinationName.Length;


            string[] DestCat = Request.Form["DestCat[]"];
            int cnt2 = DestCat.Length;



            string[] ORoadKM = Request.Form["ORoadKM[]"];
            int cnt3 = ORoadKM.Length;


            string[] OJourneyHR = Request.Form["OJourneyHR[]"];
            int cnt4 = OJourneyHR.Length;



            string[] MDestinationName = Request.Form["MDestinationName[]"];
            int cnt5 = MDestinationName.Length;


            string[] MInformation = Request.Form["MInformation[]"];
            int cnt6 = MInformation.Length;



            string[] MRoadKM = Request.Form["MRoadKM[]"];
            int cnt7 = MRoadKM.Length;


            string[] MJourneyHR = Request.Form["MJourneyHR[]"];
            int cnt8 = MJourneyHR.Length;




            string[] OpDestinationName = Request.Form["OpDestinationName[]"];
            int cnt9 = OpDestinationName.Length;


            string[] OpInformation = Request.Form["OpInformation[]"];
            int cnt10 = OpInformation.Length;



            string[] OpRoadKM = Request.Form["OpRoadKM[]"];
            int cnt11 = OpRoadKM.Length;


            string[] OpJourneyHR = Request.Form["OpJourneyHR[]"];
            int cnt12 = OpJourneyHR.Length;



            string[] EDestinationName = Request.Form["EDestinationName[]"];
            int cnt13 = EDestinationName.Length;


            string[] EInformation = Request.Form["EInformation[]"];
            int cnt14 = EInformation.Length;



            string[] ERoadKM = Request.Form["ERoadKM[]"];
            int cnt15 = ERoadKM.Length;


            string[] EJourneyHR = Request.Form["EJourneyHR[]"];
            int cnt16 = EJourneyHR.Length;

            if (LocID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_ConnectedDestination");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varPlace", DestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varCategory", DestCat[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ORoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt5; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_MustSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", MDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", MInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ORoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt9; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_OptionalSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", OpDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", OpInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(OpRoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OpJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt13; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_ExcusrionSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", EDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", EInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ERoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(EJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }
            }


            if (LocID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Locations");
        }


        [HttpPost]
        public IActionResult AddVisa(VisaAssistance model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Console.WriteLine("hi");
            
            string Continent = Request.Form["Continent"];
            string CountryName = Request.Form["CountryName"];
            string GuestProfession = Request.Form["GuestProfession"];
            string Fees = Request.Form["Fees"];
            string VisaType = Request.Form["VisaType"];

            string Duration = Request.Form["Duration"];

            string SOP = Request.Form["SOP"];
            string ImportantNote = Request.Form["ImportantNote"];

            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_VisaAssistance";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_VisaAssistance");
                cmdB.Parameters.AddWithValue("@varContinent", Continent);
                cmdB.Parameters.AddWithValue("@varCountryName", CountryName);
                cmdB.Parameters.AddWithValue("@varGuestProfession", GuestProfession);
                cmdB.Parameters.AddWithValue("@varVisaType", VisaType);

                cmdB.Parameters.AddWithValue("@varFees", int.Parse(Fees));
                cmdB.Parameters.AddWithValue("@varDuration", Duration);
                cmdB.Parameters.AddWithValue("@varSOP", SOP);
                cmdB.Parameters.AddWithValue("@varImpNote", ImportantNote);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            
            return RedirectToAction("VisaAssistance");
        }


        [HttpPost]
        public IActionResult AddHotelDetails(Locations model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            string Name = Request.Form["Name"];
            string GroupOf = Request.Form["GroupOf"];
            string SectorName = Request.Form["SectorName"];

            string Address1 = Request.Form["Address1"];
            string Address2 = Request.Form["Address2"];
            string Address1T = Request.Form["Address1T"];
            string Address1C = Request.Form["Address1C"];
            string Address2C = Request.Form["Address2C"];
            string Address2T = Request.Form["Address2T"];


            string Star = Request.Form["Star"];
            string HotelCat = Request.Form["HotelCat"];
            string ContactName = Request.Form["ContactName"];
            string ContactMobile = Request.Form["ContactMobile"];
            string ContactDesignation = Request.Form["ContactDesignation"];
            string ContactLandLine = Request.Form["ContactLandLine"];
            string ContactMail = Request.Form["ContactMail"];
            string ContactPerMob = Request.Form["ContactPerMob"];
            string ContactWorkP = Request.Form["ContactWorkProfile"];

            string ReservationContact = Request.Form["ReservationContact"];
            string IsFour = Request.Form["IsFour"];
            string CompanyFrom = Request.Form["CompanyFrom"];
            string Meal = Request.Form["Meal"];
            string Category = Request.Form["Category"];
            string ReferenceName = Request.Form["ReferenceName"];
            string AccrediationName = Request.Form["AccrediationName"];


            string OwnVisit = Request.Form["OwnVisit"];
            string RoomTypes = Request.Form["RoomTypes"];
            string Grade = Request.Form["Grade"];
            Double ExtraAdultCharges = Double.Parse(Request.Form["ExtraAdultCharges"]);
            Double ExtraChildCharges = Double.Parse(Request.Form["ExtraChildCharges"]);
            Double ExtraMealCharges = Double.Parse(Request.Form["ExtraMealCharges"]);



            int HID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Hotel_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Hotels");
                cmdB.Parameters.AddWithValue("@varName", Name);
                cmdB.Parameters.AddWithValue("@varGroupOf", GroupOf);
                cmdB.Parameters.AddWithValue("@varSector", SectorName);
                cmdB.Parameters.AddWithValue("@varAddress1", Address1);

                cmdB.Parameters.AddWithValue("@varAddress1Country", Address1C);
                cmdB.Parameters.AddWithValue("@varAddress1Town", Address1T);
                cmdB.Parameters.AddWithValue("@varAddress2", Address2);
                cmdB.Parameters.AddWithValue("@varAddress2Country", Address2C);
                cmdB.Parameters.AddWithValue("@varAddress2Town", Address2T);
                cmdB.Parameters.AddWithValue("@varStarCategory", Star);
                cmdB.Parameters.AddWithValue("@varHotelCategory", HotelCat);
                cmdB.Parameters.AddWithValue("@varContactName", ContactName);
                cmdB.Parameters.AddWithValue("@varContactDesignation", ContactDesignation);
                cmdB.Parameters.AddWithValue("@varContactMobile", ContactMobile);
                cmdB.Parameters.AddWithValue("@varContactLandLine", ContactLandLine);
                cmdB.Parameters.AddWithValue("@varContactMailID", ContactMail);
                cmdB.Parameters.AddWithValue("@varContactPerMob", ContactPerMob);
                cmdB.Parameters.AddWithValue("@varContactWorProfile", ContactWorkP);

                cmdB.Parameters.AddWithValue("@varReservationContact", ReservationContact);

                cmdB.Parameters.AddWithValue("@varMealPlan", Meal);
                cmdB.Parameters.AddWithValue("@varIsFourPerson", IsFour);
                cmdB.Parameters.AddWithValue("@varCompanyFrom", CompanyFrom);
                cmdB.Parameters.AddWithValue("@varCategory", Category);

                cmdB.Parameters.AddWithValue("@varReference", ReferenceName);
                cmdB.Parameters.AddWithValue("@varAccrediation", AccrediationName);
                cmdB.Parameters.AddWithValue("@varOwnVisit", OwnVisit);
                cmdB.Parameters.AddWithValue("@varRoomType", RoomTypes);
                cmdB.Parameters.AddWithValue("@varGrade", Grade);
                cmdB.Parameters.AddWithValue("@varTarrifPDF", null);
                cmdB.Parameters.AddWithValue("@varExtraAdultCharges", ExtraAdultCharges);
                cmdB.Parameters.AddWithValue("@varExtraChildCharges", ExtraChildCharges);
                cmdB.Parameters.AddWithValue("@varExtraMealCharges", ExtraMealCharges);


                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    HID = int.Parse(drB["HID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            string[] BankName = Request.Form["BankName[]"];
            int cnt = BankName.Length;


            string[] AccountNo = Request.Form["AccountNo[]"];
            int cnt2 = AccountNo.Length;



            string[] IFSC = Request.Form["IFSC[]"];
            int cnt3 = IFSC.Length;


            string[] Branch = Request.Form["Branch[]"];
            int cnt4 = Branch.Length;



            if (HID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Hotel_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_HotelBank");
                        cmdB.Parameters.AddWithValue("@varHID", HID);
                        cmdB.Parameters.AddWithValue("@varName", BankName[i]);
                        cmdB.Parameters.AddWithValue("@varAccNo", AccountNo[i]);
                        cmdB.Parameters.AddWithValue("@varIFSC", IFSC[i]);
                        cmdB.Parameters.AddWithValue("@varBranch", Branch[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



            }


            if (HID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Hotels");
        }


        [HttpPost]
        public IActionResult AddDMCDetails(DMC model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            string CompanyName = Request.Form["CompanyName"];
            string GroupOf = Request.Form["GroupOf"];
            string SectorName = Request.Form["SectorName"];

            string Address1 = Request.Form["Address1"];
            string Address2 = Request.Form["Address2"];
            string SpecificSP = Request.Form["SpecificSp"];

            string ServiceIn = Request.Form["ServiceIn"];
         


            string EmergencyContact = Request.Form["EmergencyContact"];


            string CompanyFrom = Request.Form["CompanyFrom"];

            string ReferenceName = Request.Form["ReferenceName"];
            string AccrediationName = Request.Form["AccrediationName"];


            string OwnFleets = Request.Form["OwnFleets"];
            string Grade = Request.Form["Grade"];


            int DID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Add_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_DMC");
                cmdB.Parameters.AddWithValue("@varName", CompanyName);
                cmdB.Parameters.AddWithValue("@varGroupOf", GroupOf);
                cmdB.Parameters.AddWithValue("@varSecWork", SectorName);
                cmdB.Parameters.AddWithValue("@varAddress1", Address1);

                cmdB.Parameters.AddWithValue("@varAddress2", Address2);

                cmdB.Parameters.AddWithValue("@varSpecificSP", SpecificSP);
                cmdB.Parameters.AddWithValue("@varServiceIn", ServiceIn);
                cmdB.Parameters.AddWithValue("@varEstFrom", CompanyFrom);
                cmdB.Parameters.AddWithValue("@varOwnFleets", OwnFleets);

                cmdB.Parameters.AddWithValue("@varReference", ReferenceName);
                cmdB.Parameters.AddWithValue("@varAccrediation", AccrediationName);



                cmdB.Parameters.AddWithValue("@varEmerContact", EmergencyContact);



                cmdB.Parameters.AddWithValue("@varGrade", Grade);


                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    DID = int.Parse(drB["DID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            string[] BankName = Request.Form["BankName[]"];
            int cnt = BankName.Length;


            string[] AccountNo = Request.Form["AccountNo[]"];
            int cnt2 = AccountNo.Length;



            string[] IFSC = Request.Form["IFSC[]"];
            int cnt3 = IFSC.Length;


            string[] Branch = Request.Form["Branch[]"];
            int cnt4 = Branch.Length;

            string[] ContactType = Request.Form["ContactType[]"];

            string[] ContactName = Request.Form["ContactName[]"];
            int cnt5 = ContactName.Length;

            string[] ContactMobile = Request.Form["ContactMobile[]"];
            string[] ContactDesignation = Request.Form["ContactDesignation[]"];
            string[] ContactLandLine = Request.Form["ContactLandLine[]"];
            string[] ContactMail = Request.Form["ContactMail[]"];
            string[] ContactPerMob = Request.Form["ContactPerMob[]"];
            string[] ContactWorkP = Request.Form["ContactWorkProfile[]"];

            if (DID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_DMCBankDetails");
                        cmdB.Parameters.AddWithValue("@varDID", DID);
                        cmdB.Parameters.AddWithValue("@varName", BankName[i]);
                        cmdB.Parameters.AddWithValue("@varAccNo", AccountNo[i]);
                        cmdB.Parameters.AddWithValue("@varIFSC", IFSC[i]);
                        cmdB.Parameters.AddWithValue("@varBranch", Branch[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }


                for (int i = 0; i < cnt5; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_DMCContacts");
                        cmdB.Parameters.AddWithValue("@varDID", DID);
                        cmdB.Parameters.AddWithValue("@varDesignation", ContactDesignation[i]);
                        cmdB.Parameters.AddWithValue("@varType", ContactType[i]);

                        cmdB.Parameters.AddWithValue("@varName", ContactName[i]);
                        cmdB.Parameters.AddWithValue("@varMobile", ContactMobile[i]);
                        cmdB.Parameters.AddWithValue("@varOfficeLandLine", ContactLandLine[i]);
                        cmdB.Parameters.AddWithValue("@varMailID", ContactMail[i]);
                        cmdB.Parameters.AddWithValue("@varPersonalMob", ContactPerMob[i]) ;
                        cmdB.Parameters.AddWithValue("@varWorkProfile", ContactWorkP[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



            }


            if (DID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("DMC");
        }


        public IActionResult ViewHotel(int? ID)
        {
            List<Hotels>  Hotels =  new List<Hotels>();
            List<Hotels> Bank = new List<Hotels>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Hotels_By_ID");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Hotels.Add(new Hotels
                {
                    ID = int.Parse(drB["ID"].ToString()),
                    HID = int.Parse(drB["HID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    Name = drB["Name"].ToString(),
                    SectorName = drB["Sector"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address1T = drB["Address1Town"].ToString(),
                    Address1C = drB["Address1Country"].ToString(),
                    Address2T = drB["Address2Town"].ToString(),
                    Address2C = drB["Address2Country"].ToString(),
                    Address2 = drB["Address2"].ToString(),
                    HotelCat = drB["HotelCategory"].ToString(),
                    ExtraAdultCharges = Double.Parse(drB["ExtraAdultCharges"].ToString()),
                    ExtraChildCharges = Double.Parse(drB["ExtraChildCharges"].ToString()),
                    ExtraMealCharges = Double.Parse(drB["ExtraMealCharges"].ToString()),

                    Star = drB["StarCategory"].ToString(),
                    ContactDesignation = drB["ContactDesignation"].ToString(),
                    ContactMail = drB["ContactMailID"].ToString(),
                    ContactMobile = drB["ContactMobile"].ToString(),
                    ContactLandLine = drB["ContactLandLine"].ToString(),
                    ContactPerMob = drB["ContactPerMob"].ToString(),
                    ContactWorkProfile = drB["ContactWorProfile"].ToString(),
                    Meal = drB["MealPlan"].ToString(),
                    IsFour = drB["IsFourPerson"].ToString(),
                    CompanyFrom = int.Parse(drB["CompanyFrom"].ToString()),
                    CategoryName = drB["Category"].ToString(),
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["Reservation Contact"].ToString(),
                    OwnVisit = drB["OwnVisit"].ToString(),
                    RoomTypes = drB["RoomType"].ToString(),
                    Grade = drB["Grade"].ToString(),


                });
            }

            ViewBag.Hotels = Hotels;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_HotelBankDetails_By_ID");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

           drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Bank.Add(new Hotels
                {
                    HID = int.Parse(drB["HID"].ToString()),
                
                    BankName = drB["HotelBankName"].ToString(),
                    AccountNo = drB["AccNo"].ToString(),
                    Branch = drB["Branch"].ToString(),
                    IFSC = drB["IFSC"].ToString(),


                });
            }

            ViewBag.Bank = Bank;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



  
            return View();
        }


        public IActionResult DeleteLocation(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Delete_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Location");
            cmdB.Parameters.AddWithValue("@varLocID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Locations");
        }


        public IActionResult DeleteHotel(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Hotel");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Hotels");
        }

        public IActionResult DeleteDMC(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Delete_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_DMC");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("DMC");
        }


        public IActionResult DeleteVisa(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_VisaAssistance");
            cmdB.Parameters.AddWithValue("@varVAID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("VisaAssistance");
        }

        public IActionResult CloseProject(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            try
            {
                conn.Open();
                SqlCommand cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TravelHub_Update_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Close_Project");
                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);

                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("ViewProject", new { ID = ID });

            }
            finally
            {

                conn.Close();
            }
            Message = "Success";

            return RedirectToAction("ViewProject", new { ID = ID });
        }

        public async Task<IActionResult> DeleteFile(int? ID, int P)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            String FileName = "";
            try
            {
                conn.Open();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TravelHub_DELETE_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "DELETE_File");
                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);

                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("ViewProject", new { ID = P });

            }
            finally
            {

                conn.Close();
            }

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_Display_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "View_Project_Files_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                FileName = drB["FileName"].ToString();
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            CloudBlockBlob blob;
            string connStr = CDNSettings.Conn;

            CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer container = client.
            GetContainerReference("projectfiles");
            if (await container.ExistsAsync())
            {
                blob = container.GetBlockBlobReference(FileName);

                if (await blob.ExistsAsync())
                {
                    CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                    if (snapshot.SnapshotTime != null)
                    {
                        await blob.DeleteAsync();
                    }

                }
            }

            Message = "Success";

            return RedirectToAction("ViewProject", new { ID = P });
        }



        [HttpPost]
        public IActionResult EditUnitDetails(Unit model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId


            string Name = Request.Form["Name"];
            int ID = int.Parse(Request.Form["UnitID"]);
            if (Name == "")
            {
                ModelState.AddModelError("Error", "Field can not be blanked");
                return View();

            }
            int Exist = 0;

            try
            {
                SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
                conn.Open();
                SqlCommand cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TravelHub_Update_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_Unit");
                cmdB.Parameters.AddWithValue("@varUnit", Name);
                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);
                cmdB.Parameters.AddWithValue("@varID", ID);


                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    Exist = int.Parse(drB["Exist"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {


            }
            if (Exist == 1)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Unit");
        }


        public IActionResult DeleteCompany(int? id)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_SP_INVOICE";
            cmdB.Parameters.AddWithValue("@varStepName", "DELETE_Company");
            cmdB.Parameters.AddWithValue("@Id", id);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            TempData["Message"] = "";
            TempData["Message"] = "Company deleted succesfully";
            return RedirectToAction("CompanyDetails");
        }


        public IActionResult DeleteInvoice(int? id)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_SP_INVOICE";
            cmdB.Parameters.AddWithValue("@varStepName", "DELETE_Invoice");
            cmdB.Parameters.AddWithValue("@Id", id);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            TempData["Message"] = "";
            TempData["Message"] = "Invoice deleted succesfully";
            return RedirectToAction("InvoiceDetails");
        }
        #endregion


    

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
