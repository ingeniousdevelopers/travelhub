﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using TravelHub.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using TravelHub.Utility;
using Microsoft.AspNetCore.Http;

namespace TravelHub.Services
{

    public class AdminAccess
    {

        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public AdminAccess(UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager, ApplicationDbContext _db, IConfiguration config, IHttpContextAccessor httpContextAccessor)
        {
            this._db = _db;
            _config = config;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;


        }

        public List<String> GetAdminAccess()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
  
            List<String> ModuleList = new List<String>();

            //try
            //{
            //    conn.Open();

            //    cmdB = new SqlCommand();
            //    cmdB.CommandType = CommandType.StoredProcedure;
            //    cmdB.CommandText = "ManageAdmin_Access";
            //    cmdB.Parameters.AddWithValue("@varStepName", "Select_Admin_Access_By_ID");
            //    cmdB.Parameters.AddWithValue("@varUserID", userId);

            //    cmdB.Connection = conn;

            //    SqlDataReader drB = cmdB.ExecuteReader();



            //    while (drB.Read())
            //    {
            //        ModuleList.Add(

            //            drB["ModuleName"].ToString()

            //        );
            //    }

            //    drB.Dispose();
            //    cmdB.Dispose();
            //}
            //catch (Exception e)
            //{

            //    Console.WriteLine(e.ToString());
            //}
            //finally
            //{
            //    conn.Close();
            //}

            return ModuleList;
        }

        public List<String> GetRoles()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

            List<String> ModuleList = new List<String>();

            //try
            //{
            //    conn.Open();

            //    cmdB = new SqlCommand();
            //    cmdB.CommandType = CommandType.StoredProcedure;
            //    cmdB.CommandText = "ManageAdmin_Access";
            //    cmdB.Parameters.AddWithValue("@varStepName", "View_Role");
            //    cmdB.Parameters.AddWithValue("@varUserID", userId);

            //    cmdB.Connection = conn;

            //    SqlDataReader drB = cmdB.ExecuteReader();



            //    while (drB.Read())
            //    {
            //        if (int.Parse(drB["RoleId"].ToString()) == 2) {

            //            ModuleList.Add(
            //                "Office"
            //             );
            //        }

            //        if (int.Parse(drB["RoleId"].ToString()) == 3)
            //        {

            //            ModuleList.Add(
            //                "Site"
            //             );
            //        }

            //        if (int.Parse(drB["RoleId"].ToString()) == 1)
            //        {

            //            ModuleList.Add(
            //                "Admin"
            //             );
            //        }



            //    }

            //    drB.Dispose();
            //    cmdB.Dispose();
            //}
            //catch (Exception e)
            //{

            //    Console.WriteLine(e.ToString());
            //}
            //finally
            //{
            //    conn.Close();
            //}

            return ModuleList;
        }

        public int CheckSuperAdminAccess()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

            var LoggedIn = _db.Users.Where(u => u.Id == Int32.Parse(userId));
            var email = LoggedIn.First().Email;

            List<String> ModuleList = new List<String>();

            int allowed = 1;
            //try
            //{
            //    conn.Open();

            //    cmdB = new SqlCommand();
            //    cmdB.CommandType = CommandType.StoredProcedure;
            //    cmdB.CommandText = "ManageAdmin_Access";
            //    cmdB.Parameters.AddWithValue("@varStepName", "Email_Exist");
            //    cmdB.Parameters.AddWithValue("@varEmail", email);

            //    cmdB.Connection = conn;

            //    SqlDataReader drB = cmdB.ExecuteReader();



            //    while (drB.Read())
            //    {

            //        allowed = int.Parse(drB["Exist"].ToString());


            //    }

            //    drB.Dispose();
            //    cmdB.Dispose();
            //}
            //catch (Exception e)
            //{

            //    Console.WriteLine(e.ToString());
            //}
            //finally
            //{
            //    conn.Close();
            //}

            return allowed;
        }







    }
}
