﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Nira.Services
{
    public interface IEmailAttachment
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage, string fileName);
    }
}
