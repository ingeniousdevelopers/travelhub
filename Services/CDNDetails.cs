﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TravelHub.Services
{
    public class CDNDetails
    {

        public string CDNUrl { get; set; }
        public string BlobUrl { get; set; }
        public string Conn { get; set; }


    }
}
